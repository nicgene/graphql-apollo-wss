import { IResolvers, makeExecutableSchema } from 'graphql-tools';
// import { checkAuth } from '../checkAuth';
// import { response } from '../response';
import { PubSub } from 'graphql-subscriptions';
import Uuid from '@dupkey/uuid';

export const pubsub = new PubSub();

const CHAT_ADDED = 'CHAT_ADDED';

const typeDefs = `
  scalar Date

  input ChatInput {
    username: String!
    content: String!
  }

  type Chat {
    id: ID!
    username: String!
    content: String!
    created: Date!
    updated: Date
  }

  type Subscription {
    chatAdded: Chat
  }

  type Mutation {
    addChat(input: ChatInput!): Chat
  }

  type Query {
    chatRoll: [Chat]
  }
`;

const resolvers: IResolvers = {
  Query: {
    chatRoll: async (
      _: any, {}, { context }: any
    ): Promise<any> => {
      // let userService: UserService = context.container.get('userService');
      // let payload = await userService.fetch(id);
      // return response(payload);
      return [];
    },
  },
  Mutation: {
    addChat: async (
      _: any, { input }: { input: {
        username: string,
        content: string
      } }, { context }: any
    ): Promise<any> => {
    //   let userService: UserService = context.container.get('userService');
    //   let payload = await userService.create(
    //     input.name,
    //     input.email,
    //     input.password
    //   );

      let chat = {
        id: Uuid.v4(),
        username: input.username,
        content: input.content,
        created: new Date(),
        updated: null,
      };

      pubsub.publish(CHAT_ADDED, { chatAdded: chat });

      // return response(payload);
      return chat;
    }
  },
  Subscription: {
    chatAdded: {
      subscribe: () => pubsub.asyncIterator([CHAT_ADDED]),
    }
  }
}

export default makeExecutableSchema({ typeDefs, resolvers });