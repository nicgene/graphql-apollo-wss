import Uuid from '@dupkey/uuid';
import * as mariadb from 'mariadb';
import AuthTokenEntity from './AuthTokenEntity';

export default class AuthTokenRepository
{
  private database: mariadb.Connection;

  constructor(database: mariadb.Connection)
  {
    this.database = database;
  }

  private createInstance(authToken: any): AuthTokenEntity
  {
    return new AuthTokenEntity(
      Uuid.fromBuffer(authToken.id),
      Uuid.fromBuffer(authToken.userId),
      authToken.accessToken,
      authToken.refreshToken,
      authToken.created,
    );
  }

  public async deleteOneByAccessToken(accessToken: string): Promise<object>
  {
    let sql = " \
      DELETE FROM `account_authToken` \
      WHERE `account_authToken`.`accessToken` = :accessToken \
    ";

    let values = { accessToken }

    return await this.database.query(sql, values);
  }

  public async deleteOneByRefreshToken(refreshToken: string): Promise<object>
  {
    let sql = " \
      DELETE FROM `account_authToken` \
      WHERE `account_authToken`.`refreshToken` = :refreshToken \
    ";

    let values = { refreshToken }

    return await this.database.query(sql, values);
  }

  public async fetchOneByAccessToken(accessToken: string): Promise<AuthTokenEntity | null>
  {
    let sql = " \
      SELECT * \
      FROM `account_authToken` \
      WHERE `account_authToken`.`accessToken` = :accessToken \
    ";
    let result = await this.database.query(sql, { accessToken });
    if (result.length === 1) {
      return this.createInstance(result[0]);
    }

    return null;
  }

  public async save(authToken: AuthTokenEntity): Promise<object>
  {
    let sql = " \
      INSERT INTO `account_authToken` \
      (id, userId, accessToken, refreshToken, created) VALUES \
      (:id, :userId, :accessToken, :refreshToken, :created) \
    ";

    let values = {
      id: authToken.getId().getBuffer(),
      userId: authToken.getUserId().getBuffer(),
      accessToken: authToken.getAccessToken(),
      refreshToken: authToken.getRefreshToken(),
      created: authToken.getCreated()
    }

    return await this.database.query(sql, values);
  }
}