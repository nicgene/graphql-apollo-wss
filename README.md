# graphql-wss

## Start the development server

```bash
yarn run ts-node src/server.ts
```

## Respawn server on change

```bash
yarn run ts-node-dev --respawn --transpileOnly src/index.ts
```
