import { IResolvers, makeExecutableSchema } from 'graphql-tools';
// import { checkAuth } from '../checkAuth';
import UserService from '../domain/Account/User/UserService';
// import { response } from '../response';

const typeDefs = `
  scalar Date

  type ActivateUserPayload {
    id: ID!
  }

  input RegisterUserInput {
    name: String!
    email: String!
    password: String!
  }

  type User {
    id: ID!
    name: String!
    created: Date!
    updated: Date
  }

  type Mutation {
    activateUser(id: ID!): ActivateUserPayload
    registerUser(input: RegisterUserInput!): User
  }

  type Query {
    userById(id: ID!): User
    userCurrent: User
  }
`;

const resolvers: IResolvers = {
  Query: {
    userById: async (
      _: any, { id }: { id: string }, { context }: any
    ): Promise<any> => {
      let userService: UserService = context.container.get('userService');
      let payload = await userService.fetch(id);
      // return response(payload);
      return;
    },

    userCurrent: async (
      _: any, {}, { context, headers }: any
    ): Promise<any> => {
      // let userPayload = await checkAuth(context, headers);
      // return response(userPayload);
      return;
    }
  },
  Mutation: {
    activateUser: async (
      _: any, { id }: { id: string }, { context }: any
    ): Promise<any> => {
      let userService: UserService = context.container.get('userService');
      let payload = await userService.updateActivated(id);
      // return response(payload);
      return;
    },

    registerUser: async (
      _: any, { input }: { input: {
        name: string,
        email: string,
        password: string
      } }, { context }: any
    ): Promise<any> => {
      let userService: UserService = context.container.get('userService');
      let payload = await userService.create(
        input.name,
        input.email,
        input.password
      );

      // pubsub.publish(USER_ADDED, { userAdded: { foo: 'bar' } });

      // return response(payload);
      return;
    }
  }
}

export default makeExecutableSchema({ typeDefs, resolvers });