import { IResolvers, makeExecutableSchema } from 'graphql-tools';
import AuthTokenService from '../domain/Account/AuthToken/AuthTokenService';
// import { response } from '../response';

const typeDefs = `
  scalar Date

  type AuthToken {
    id: ID!
    userId: ID!
    accessToken: String!
    refreshToken: String
    created: Date
  }

  type LogoutUserPayload {
    accessToken: String!
  }

  input SigninUserInput {
    email: String!
    password: String!
  }

  type Mutation {
    logoutUser(accessToken: String!): LogoutUserPayload
    signinUser(input: SigninUserInput!): AuthToken
  }

  type Query {
    required: Boolean
  }
`;

const resolvers: IResolvers = {
  Mutation: {
    logoutUser: async (
      _: any, { accessToken }: { accessToken: string }, { context }: any
    ): Promise<any> => {
      let authTokenService: AuthTokenService = context.container.get('authTokenService');
      let payload = await authTokenService.userLogout(accessToken);
      // return response(payload);
      return;
    },

    signinUser: async (
      _: any, { input }: { input: {
        email: string,
        password: string
      } }, { context }: any
    ): Promise<any> => {
      let authTokenService: AuthTokenService = context.container.get('authTokenService');
      let payload = await authTokenService.userSignin(
        input.email,
        input.password
      );

      // return response(payload);
      return;
    }
  }
}

export default makeExecutableSchema({ typeDefs, resolvers });