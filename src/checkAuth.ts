// import Jwt from '@dupkey/jwt';
// import { Payload, PayloadInterface, PayloadStatus } from '@dupkey/payload';
// import { AuthenticationError } from 'apollo-server';
// import AuthTokenService from './domain/Account/AuthToken/AuthTokenService';
// import UserService from './domain/Account/User/UserService';

// export const checkAuth = async (context: any, headers: any, required: Boolean = true): Promise<PayloadInterface> => {
//   let payload = (new Payload()).setInput({ context, required });

//   let token = headers.authorization || headers.Authorization;
//   if (token === undefined && required === true) {
//     throw new AuthenticationError('You must supply a JWT for authentication!');
//   }

//   if (token === undefined && required === false) {
//     return payload.setStatus(PayloadStatus.ACCEPTED).setOutput(null);
//   }

//   token = token.replace('Bearer ', '');
//   let jsonWebToken: Jwt = context.container.get('jsonWebToken');
//   let decoded = jsonWebToken.verifyAccessToken(token)

//   let authTokenService: AuthTokenService = context.container.get('authTokenService');
//   let authToken = await authTokenService.checkAuth(token);
//   if (authToken.getStatus() === PayloadStatus.NOT_FOUND) {
//     throw new AuthenticationError('JWT was revoked!');
//   }

//   if (authToken.getOutput().getUserId().toString() !== decoded.id) {
//     throw new AuthenticationError('User/token mismatch!');
//   }

//   let userService: UserService = context.container.get('userService');
//   let user = await userService.fetch(decoded.id);
//   if (user.getStatus() === PayloadStatus.NOT_FOUND) {
//     throw new AuthenticationError('Invalid user!');
//   }

//   return user;
// };