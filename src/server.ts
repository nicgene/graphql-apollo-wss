import { ApolloServer } from 'apollo-server';
import schema from './schema';

const server = new ApolloServer({
  schema,
  context: async ({ req, connection }) => {
    if (connection) {
      // check connection for metadata
      return connection.context;
    } else {
      // check from req
      // const token = req.headers.authorization || "";
      let token = req.headers.authorization || req.headers.Authorization || '';
      return { token };
    }
  },
});

server.listen().then(({ url, subscriptionsUrl }: { url: string, subscriptionsUrl: string }) => {
  console.log(`🚀 Server ready at ${url}`);
  console.log(`🚀 Subscriptions ready at ${subscriptionsUrl}`);
});