import Uuid from '@dupkey/uuid';

export default class AuthTokenEntity
{
  private id: Uuid;
  private userId: Uuid;
  private accessToken: string;
  private created: Date;
  private refreshToken: string | null;

  constructor(
    id: Uuid,
    userId: Uuid,
    accessToken: string,
    created: Date,
    refreshToken: string | null = null
  ) {
    this.id = id;
    this.userId = userId;
    this.accessToken = accessToken;
    this.created = created;
    this.refreshToken = refreshToken;
  }

  public getId(): Uuid
  {
    return this.id;
  }

  public getUserId(): Uuid
  {
    return this.userId;
  }

  public getAccessToken(): string
  {
    return this.accessToken;
  }

  public getCreated(): Date
  {
    return this.created;
  }

  public getRefreshToken(): string | null
  {
    return this.refreshToken;
  }
}