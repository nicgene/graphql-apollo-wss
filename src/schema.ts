import { FilterRootFields, mergeSchemas, transformSchema } from 'graphql-tools';
import authTokenSchema from './graphql/authTokenSchema';
import scalarTypeDate from './graphql/scalarType/scalarTypeDate';
import userSchema from './graphql/userSchema';
import chatSchema from './graphql/chatSchema';

const overrideTypeDefs = `
  type AuthToken {
    id: ID!
    user: User
    accessToken: String!
    refreshToken: String
    created: Date
  }
`;

const transformedUserSchema = transformSchema(userSchema, [
  new FilterRootFields((operation, fieldName, field) => {
    if (fieldName === 'userById') {
      return false;
    }
    return true;
  })
]);

export default mergeSchemas({
  schemas: [
    scalarTypeDate,
    authTokenSchema,
    chatSchema,
    transformedUserSchema,  
    overrideTypeDefs
  ],
  resolvers: {
    AuthToken: {
      user: {
        fragment: 'fragment AuthTokenFragment on AuthToken { userId }',
        resolve(authToken: any, {}, context: any, info: any) {
          return info.mergeInfo.delegateToSchema({
            schema: userSchema,
            operation: 'query',
            fieldName: 'userById',
            args: {
              id: String(authToken.userId)
            },
            context,
            info
          });
        }
      }
    },
  },
});