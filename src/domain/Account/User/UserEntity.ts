import { Email } from '@dupkey/mail';
import Password from '@dupkey/password';
import Uuid from '@dupkey/uuid';

export default class UserEntity
{
  private id: Uuid;
  private name: string;
  private email: Email;
  private password: Password;
  private created: Date;
  private updated: Date | null;
  private activated: Date | null;
  private banned: Date | null;

  constructor(
    id: Uuid,
    name: string,
    email: Email,
    password: Password,
    created: Date,
    updated: Date | null = null,
    activated: Date | null = null,
    banned: Date | null = null
  ) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.password = password;
    this.created = created;
    this.updated = updated;
    this.activated = activated;
    this.banned = banned;
  }

  public getId(): Uuid
  {
    return this.id;
  }

  public getName(): string
  {
    return this.name;
  }

  public setName(name: string): UserEntity
  {
    this.name = name;
    return this;
  }

  public getEmail(): Email
  {
    return this.email;
  }

  public getPassword(): Password
  {
    return this.password;
  }

  public setPassword(password: Password): UserEntity
  {
    this.password = password;
    return this;
  }

  public getCreated(): Date
  {
    return this.created;
  }

  public getUpdated(): Date | null {
    return this.updated;
  }

  public setUpdated(updated: Date): UserEntity
  {
    this.updated = updated;
    return this;
  }

  public getActivated(): Date | null
  {
    return this.activated;
  }

  public setActivated(activated: Date): UserEntity
  {
    this.activated = activated;
    return this;
  }

  public getBanned(): Date | null
  {
    return this.banned;
  }

  public setBanned(banned: Date): UserEntity
  {
    this.banned = banned;
    return this;
  }
}
