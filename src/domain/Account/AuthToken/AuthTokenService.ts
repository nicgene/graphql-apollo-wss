import Jwt from '@dupkey/jwt';
import Password from '@dupkey/password';
import { Payload, PayloadInterface, PayloadStatus } from '@dupkey/payload';
import Uuid from '@dupkey/uuid';
import UserRepository from '../User/UserRepository';
import AuthTokenEntity from './AuthTokenEntity';
import AuthTokenRepository from './AuthTokenRepository';
import AuthTokenValidator from './AuthTokenValidator';

export default class AuthTokenService
{
  private authTokenRepository: AuthTokenRepository;
  private authTokenValidator: AuthTokenValidator;
  private jsonWebToken: Jwt;
  private userRepository: UserRepository;

  constructor(
    authTokenRepository: AuthTokenRepository,
    authTokenValidator: AuthTokenValidator,
    jsonWebToken: Jwt,
    userRepository: UserRepository
  ) {
    this.authTokenRepository = authTokenRepository;
    this.authTokenValidator = authTokenValidator;
    this.jsonWebToken = jsonWebToken;
    this.userRepository = userRepository;
  }

  public async checkAuth(accessToken: string): Promise<PayloadInterface>
  {
    let payload = (new Payload).setInput({ accessToken });

    let token = await this.authTokenRepository.fetchOneByAccessToken(accessToken);
    if (token === null) {
      return payload.setStatus(PayloadStatus.NOT_FOUND);
    }

    return payload.setStatus(PayloadStatus.FOUND).setOutput(token);
  }

  public async update(refreshToken: string): Promise<PayloadInterface>
  {
    let payload = (new Payload).setInput({ refreshToken });

    let jsonWebToken = this.jsonWebToken.verifyRefreshToken(refreshToken);
    let user = await this.userRepository.fetchOneById(Uuid.fromString(jsonWebToken.id));
    if (user === null ||
        await this.authTokenRepository.deleteOneByRefreshToken(refreshToken) === null
    ) {
      return payload.setStatus(PayloadStatus.NOT_FOUND);
    }

    let authToken = new AuthTokenEntity(
      Uuid.v4(),
      user.getId(),
      this.jsonWebToken.accessToken({ id: user.getId().toString() }),
      new Date(),
      this.jsonWebToken.refreshToken({ id: user.getId().toString() })
    );

    await this.authTokenRepository.save(authToken);
    
    return payload.setStatus(PayloadStatus.CREATED).setOutput(authToken);
  }

  public async userLogout(accessToken: string): Promise<PayloadInterface>
  {
    let payload = (new Payload).setInput({ accessToken });
    
    await this.authTokenRepository.deleteOneByAccessToken(accessToken);

    return payload.setStatus(PayloadStatus.PROCESSING)
  }

  public async userSignin(email: string, password: string): Promise<PayloadInterface>
  {
    let payload = (new Payload).setInput({ email, password });

    if (this.authTokenValidator.forUserSignin(email, password) === false) {
      return payload.setStatus(PayloadStatus.NOT_VALID)
        .setErrors(this.authTokenValidator.getErrors());
    }

    let user = await this.userRepository.fetchOneByEmail(email);
    if (user === null ||
        user.getActivated() === null ||
        user.getBanned() !== null ||
        await (new Password(password)).isValid(String(user.getPassword())) === false) {
          return payload.setStatus(PayloadStatus.NOT_FOUND);          
    }

    let authToken = new AuthTokenEntity(
      Uuid.v4(),
      user.getId(),
      this.jsonWebToken.accessToken({ id: user.getId().toString() }),
      new Date()
    );

    await this.authTokenRepository.save(authToken);
    
    return payload.setStatus(PayloadStatus.CREATED).setOutput(authToken);
  }
}